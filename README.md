### Prerequisites

* Basic knowledge of C Programming
* Basic knowledge of Embedded Systems
* Basic knowledge of Cloud Applications

### Course Content

#### Day 1

* Indroduction to Internet of Things (IoT) and its Terminologies
* Various components of IoT
* Familiarization with the Hardware Kit, Software Tools and Cloud Application to be used in the training
* Development environment setup

#### Day 2

* Demonstration of a simple IoT application with LED & Button
* In depth understanding of the LED-Button application
* Source Code walk-through
* Build, flash, run and test

#### Day 3

* Basic Software Concepts
* Understanding JavaScript Object Notation (JSON)
* Understanding Network Protocols used in IoT
* Creating JSON and server communication through code

#### Day 4

* Making a Temperature Humidity Monitoring IoT Application from scratch
* List of materials required - sensors, actuators, development board
* Connecting and setting up the Hardware Kit
* Configuring and setting up the Cloud Application

#### Day 5

* Writing the program for a Temperature Humidity Monitoring IoT Application
* Testing, modifying configurations and optimizing
* Industrial Use Cases of Internet of Things
* Combining IoT with Machine Learning (ML) and Artificial Intelligence (AI)